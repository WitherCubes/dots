" Sets
lua require("sets")

" Autocommands
lua require("autocmd")

" Plugins
lua require("plugins")

" Keybindings
lua require("keybinds")

" Plugins Config
lua require("plugconfig.catppuccin")
lua require("plugconfig.lualine")
lua require("plugconfig.native-lsp")
lua require("plugconfig.npresence")
lua require("plugconfig.nvim-cmp")
