vim.g.catppuccin_flavour = "mocha"

require("catppuccin").setup({
    transparent_background = true,
    styles = {
        comments = {},
        functions = {},
        keywords = {},
        strings = {},
        variables = {},
    }
})

vim.cmd [[colorscheme catppuccin]]
