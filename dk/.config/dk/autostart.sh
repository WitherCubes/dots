#!/bin/sh

killall -9 picom dunst nm-applet blueman-applet flameshot

picom &
xrandr --dpi 96 &
xsetroot -cursor_name left_ptr &
dunst &
lxsession &
xrdb -load ~/.config/x11/xresources &
setbg ~/dots/wallpapers/arch.png &
nm-applet &
blueman-applet &
flameshot &
brightnessctl s 10 &
thunar --daemon &
$HOME/.config/polybar/dk.sh &
