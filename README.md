# dots

[![CodeBerg](https://img.shields.io/badge/Hosted_at-Codeberg-%232185D0?style=flat-square&logo=CodeBerg)](https://codeberg.org/WitherCubes/dots)

##### Table of Contents
1. [Software I use](#software)
2. [How to set up](#setup)
3. [Other than dotfiles](#misc)
4. [License](#license)

<a name="software"/>

## Software I use
| Software           | Name                                                                                     |
|--------------------|------------------------------------------------------------------------------------------|
| Linux Distribution | Arch                                                                                     |
| Window manager     | [dk](https://bitbucket.org/natemaia/dk)                                                  |
| Terminal emulator  | [Kitty](https://sw.kovidgoyal.net/kitty)                                                 |
| Terminal font      | [Hack Nerd Font](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack) |
| Shell              | zsh                                                                                      |
| Text editor        | [Neovim](https://github.com/neovim/neovim)                                               |
| RSS reader         | [Newsboat](https://github.com/newsboat/newsboat)                                         |
| Web Browser        | [Firefox](https://www.mozilla.org/firefox)                                               |
| Video player       | [mpv](https://mpv.io)                                                                    |
| Document reader    | [zathura](https://git.pwmt.org/pwmt/zathura)                                             |
| Image viewer       | [nsxiv](https://codeberg.org/nsxiv/nsxiv)                                                |
| Screen locker      | [slock](https://codeberg.org/WitherCubes/sde)                                            |

![dk-catppuccin](assets/dk-catppuccin/catppuccin.png)

<a name="setup"/>

## How to set up
Clone the repo in your HOME directory:
```bash
git clone https://github.com/WitherCubes/dots.git $HOME/dots
```

Install the dotfiles (You need GNU Stow installed):
```bash
cd $HOME/dots && make install-dotfiles
```
> Note that you need to change some things in flameshot and melonDS to your own user to fix some things.

Uninstall the dotfiles (You need GNU Stow installed):
```bash
cd $HOME/dots && make uninstall-dotfiles
```

<a name="misc"/>

## Other than dotfiles
- Run ```make help``` or ```make``` to get the list of the things that can be done using the Makefile.
- I use the Makefile to install Arch Linux packages and setup dwm, I got inspired from [this](https://github.com/masasam/dotfiles) repo.

<a name="#license"/>

## License
Licensed under the [MIT License](LICENSE).
